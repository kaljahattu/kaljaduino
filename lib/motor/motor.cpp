#include <motor.h>
#include <Arduino.h>

// Initialize the pins
void MotorManager::pinSetup() {
  pinMode(PIN_SPEED, OUTPUT);
  pinMode(PIN_DIRECTION_A, OUTPUT);
  pinMode(PIN_DIRECTION_B, OUTPUT);
}

// All getters
int MotorManager::getMotorSpeed() { return m_speed; }
MotorState MotorManager::getMotorState() { return m_state; }

//All setters
int MotorManager::setMotorSpeed(int speed) {
    m_speed = speed;

    analogWrite(PIN_SPEED, m_speed);

    return m_speed;
}
MotorState MotorManager::setMotorState(MotorState state) {
    if(m_state != MotorState::disabled) {
        m_lastState = m_state;
    }

    m_state = state;

    switch (m_state) {
        case MotorState::disabled:
            digitalWrite(PIN_DIRECTION_A, LOW);
            digitalWrite(PIN_DIRECTION_B, LOW);
            break;
        case MotorState::forward:
            digitalWrite(PIN_DIRECTION_A, HIGH);
            digitalWrite(PIN_DIRECTION_B, LOW);
            break;
        case MotorState::reverse:
            digitalWrite(PIN_DIRECTION_A, LOW);
            digitalWrite(PIN_DIRECTION_B, HIGH);
            break;
        default:
            break;
    }

    return m_state;
}

int MotorManager::isMotorEnabled() {
    if (m_state != MotorState::disabled) {
        return 1;
    } else {
        return 0;
    }
}