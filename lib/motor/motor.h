#define PIN_SPEED D4
#define PIN_DIRECTION_A D2
#define PIN_DIRECTION_B D3

enum class MotorState { forward, reverse, disabled };

class MotorManager {
    int m_speed;
    MotorState m_state;
    MotorState m_lastState;

    public:
        void pinSetup();

        int getMotorSpeed();
        MotorState getMotorState();

        int setMotorSpeed(int speed);
        MotorState setMotorState(MotorState state);

        int isMotorEnabled();
};