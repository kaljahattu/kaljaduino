#include <ArduinoBLE.h>

class HatBluetoothManager {
    const char* m_UUID;
    const char* m_name = "kaljahattu";
    BLEService  hatService {m_UUID};
    BLEBoolCharacteristic motorStateCharacteristic          {"2A20", BLERead | BLEWrite};
    BLEIntCharacteristic  motorSpeedCharacteristic          {"2B20", BLERead | BLEWrite};
    BLEBoolCharacteristic motorDirectionCharacteristic      {"2C20", BLERead | BLEWrite};

    BLEIntCharacteristic drinkSizeCharacteristic            {"2D20", BLERead | BLEWrite};
    BLEIntCharacteristic drinkSpeedCharacteristic           {"2E20", BLERead | BLEWrite};
    BLEIntCharacteristic drinkStateCharacteristic           {"2F20", BLERead | BLEWrite};
    
    public:
        HatBluetoothManager (char* UUID);
        static int initalizeBLE();
        static void pollBLE();

        bool initializeService();

        const char*& getName();
        const char*& getUUID();

        int getMotorState();
        int getMotorSpeed();
        int getMotorDirection();

        int getDrinkSize();
        int getDrinkSpeed();
        int getDrinkState();

        void setMotorState(int state);
        void setMotorSpeed(int speed);
        void setMotorDirection(int direction);

        void setDrinkSize(int size);
        void setDrinkSpeed(int speed);
        void setDrinkState(int state);

        void setMotorStateCallback    (BLECharacteristicEventHandler handler);
        void setMotorSpeedCallback    (BLECharacteristicEventHandler handler);
        void setMotorDirectionCallback(BLECharacteristicEventHandler handler);

        void setDrinkSizeCallback     (BLECharacteristicEventHandler handler);
        void setDrinkSpeedCallback    (BLECharacteristicEventHandler handler);
        void setDrinkStateCallback    (BLECharacteristicEventHandler handler);
        
};