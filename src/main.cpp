#include <Arduino.h>
#include <TimeOut.h>
#include <EasyButton.h>

#include <bluetooth.h>
#include <motor.h>
#include <drink.h>

#define ledPin LED_BUILTIN
#define drinkButtonPin D5

TimeOut drink_timeout;
EasyButton drinkButton(D5);

HatBluetoothManager bluetoothManager("19b20000-e8f2-537e-4f6c-d104768a1214");
MotorManager motorManager;
DrinkManager drinkManager;

bool is_reversed = false;

void initializeBluetooth() {
  if (!(bluetoothManager.initalizeBLE())) {
    Serial.println("starting Bluetooth® Low Energy module failed!");
    while (1);
  }

  if (!(bluetoothManager.initializeService())) {
    Serial.println("Advertizing failed!");
    while(1);
  }
}

void stopDrinking() {
  motorManager.setMotorState(MotorState::disabled);
  bluetoothManager.setDrinkState(0);
};

void updateDrinkState(int state) {
  switch (state) {
    case 0:
      Serial.println("Drink stopped");
      motorManager.setMotorState(MotorState::disabled);
      break;
    case 1:
      Serial.println("Drink started");
      motorManager.setMotorState(MotorState::forward);
      bluetoothManager.setDrinkState(2);
      drink_timeout.timeOut(drinkManager.getDrinkDelay(), stopDrinking); 
      break;
    case 2:
      Serial.println("Drink in progress");
      break;
    default:
      break;
  }
}

void motorStateWritten(BLEDevice central, BLECharacteristic characteristic) {
  Serial.println(bluetoothManager.getMotorState());
  if (bluetoothManager.getMotorState()) {
    motorManager.setMotorState(MotorState::forward);
  } else {
    motorManager.setMotorState(MotorState::disabled);
  }
}

void motorSpeedWritten(BLEDevice central, BLECharacteristic characteristic) {
  motorManager.setMotorSpeed(bluetoothManager.getMotorSpeed());
}

void motorDirectionWritten(BLEDevice central, BLECharacteristic characteristic) {
  is_reversed = bluetoothManager.getMotorDirection();
}


void drinkStateWritten(BLEDevice central, BLECharacteristic characteristic) {
  updateDrinkState(bluetoothManager.getDrinkState());
}

void drinkSizeWritten(BLEDevice central, BLECharacteristic characteristic) {
  drinkManager.setDrinkSize(bluetoothManager.getDrinkSize());
}

void drinkSpeedWritten(BLEDevice central, BLECharacteristic characteristic) {
  int speed = bluetoothManager.getDrinkSpeed();
  drinkManager.setDrinkSpeed(speed);
  motorManager.setMotorSpeed(speed);
  bluetoothManager.setMotorSpeed(speed);
}

void onDrinkButtonPressed() {
  int state = bluetoothManager.getDrinkState();

  Serial.println(state);

  if(state == 0) {
    updateDrinkState(1); //start new drink
    bluetoothManager.setDrinkState(1);
  }

}

void setupBLEHandlers() {
  bluetoothManager.setMotorStateCallback(motorStateWritten);
  bluetoothManager.setMotorSpeedCallback(motorSpeedWritten);
  bluetoothManager.setMotorDirectionCallback(motorDirectionWritten);

  bluetoothManager.setDrinkStateCallback(drinkStateWritten);
  bluetoothManager.setDrinkSizeCallback(drinkSizeWritten);
  bluetoothManager.setDrinkSpeedCallback(drinkSpeedWritten);
}

void setup() {
  Serial.begin(9600);
  // while (!Serial);

  drinkButton.begin();
  drinkButton.onPressed(onDrinkButtonPressed);

  setupBLEHandlers();
  initializeBluetooth();

  // set LED pin to output mode
  motorManager.pinSetup();
  pinMode(ledPin, OUTPUT);

  motorManager.setMotorSpeed(100);
  motorManager.setMotorState(MotorState::disabled);
}

void loop() {
  bluetoothManager.pollBLE();
  TimeOut::handler();
  drinkButton.read();
}